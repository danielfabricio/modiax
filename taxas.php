<?php $bodyclass = 'default-page not-home'; ?>
<?php include('header.php'); ?>

	<!-- BG SVG -->
	<div class="top-bg-about">
		<div class="container">
			<div class="the_bg">
			</div>
		</div>
	</div>
	<!-- BG SVG -->

	<main class="main" role="main">
		<section class="section page-top-sec">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">

						<div class="top-label w-blue grey">
							<span>Uma relação de transparência.</span>
						</div>

						<h1 class="title">
							Taxas, limites e<br/>prazos
						</h1>

					</div>
				</div>
			</div>
		</section>

		<section class="main-content">
			<div class="container custom-container">
				<div class="row">

					<div class="table-sec clear">
						<article class="article-content col-xs-12 col-lg-3">
							<h2 class="content-subtitle">Transações em Reais (BRL)</h2>
							<p class="small">Os depósitos e saques são realizados através das contas bancárias da Modiax nos seguintes bancos: Itaú, Banco do Brasil, Caixa Econômica.</p>
							<p class="featured-text">Horário de aprovação de saques e depósitos: <strong>Dias úteis das 9h às 18h</strong></p>
						</article>

						<div class="col-xs-12 col-lg-9 table-container">
							<div class="container-inner">
								<div class="Table">

									<div class="Table-col">
										<div class="Table-row-item header">Tipo de transação</div>
										<div class="Table-row-item">Depósito</div>
										<div class="Table-row-item">Saque</div>
									</div>

									<div class="Table-col">
										<div class="Table-row-item header">Taxa</div>
										<div class="Table-row-item">Sem custo</div>
										<div class="Table-row-item">R$ 10,00</div>
									</div>

									<div class="Table-col">
										<div class="Table-row-item header">Limite Mínimo</div>
										<div class="Table-row-item">R$ 500,00</div>
										<div class="Table-row-item">R$ 500,00</div>
									</div>

									<div class="Table-col">
										<div class="Table-row-item header">Limite Máximo</div>
										<div class="Table-row-item">R$ 50.000,00/Mês</div>
										<div class="Table-row-item">R$ 50.000,00/Mês</div>
									</div>

									<div class="Table-col" style="flex: 0.6;">
										<div class="Table-row-item header">Prazo</div>
										<div class="Table-row-item">1 dia útil</div>
										<div class="Table-row-item">1 dia útil</div>
									</div>

								</div>
							</div>
						</div>
						<div class="hidden-sm hidden-lg table-tag">
							<img src="images/move-table.png" alt="">
						</div>
					</div>

					<div class="table-sec clear">
						<article class="article-content col-xs-12 col-lg-3">
							<h2 class="content-subtitle">Transações em Bitcoins (BTC)</h2>
							<p class="small">Através da plataforma você consegue comprar, vender, sacar e depositar Bitcoins utilizando a sua carteira na Modiax. Todo o processo é realizado de forma automática e online.</p>
							<p class="featured-text">Horário de aprovação de saques e depósitos: <strong>24 horas por dia, 7 dias por semana.</strong></p>
						</article>

						<div class="col-xs-12 col-lg-9 table-container">
							<div class="container-inner">
								<div class="Table">

									<div class="Table-col">
										<div class="Table-row-item header">Tipo de transação</div>
										<div class="Table-row-item">Depósito</div>
										<div class="Table-row-item">Saque</div>
										<div class="Table-row-item">Compra</div>
										<div class="Table-row-item">Venda</div>
									</div>

									<div class="Table-col">
										<div class="Table-row-item header">Taxa</div>
										<div class="Table-row-item">Sem custo</div>
										<div class="Table-row-item">B 0.0001</div>
										<div class="Table-row-item">0,5%</div>
										<div class="Table-row-item">0,5%</div>
									</div>

									<div class="Table-col">
										<div class="Table-row-item header">Limite Mínimo</div>
										<div class="Table-row-item">Sem limite</div>
										<div class="Table-row-item">Sem limite</div>
										<div class="Table-row-item">Sem limite</div>
										<div class="Table-row-item">Sem limite</div>
									</div>

									<div class="Table-col">
										<div class="Table-row-item header">Limite Máximo</div>
										<div class="Table-row-item">B 80/Mês</div>
										<div class="Table-row-item">B 80/Mês</div>
										<div class="Table-row-item">Sem limite</div>
										<div class="Table-row-item">Sem limite</div>
									</div>

									<div class="Table-col">
										<div class="Table-row-item header">Prazo</div>
										<div class="Table-row-item">3 Confirmações da rede</div>
										<div class="Table-row-item">Variável <i class="icon-info"></i></div>
										<div class="Table-row-item">-</div>
										<div class="Table-row-item">-</div>
									</div>

								</div>
							</div>
						</div>
						<div class="hidden-sm hidden-lg table-tag">
							<img src="images/move-table.png" alt="">
						</div>
					</div>

				</div>
			</div>

		</section>


		<?php include('includes/account-boxes.php'); ?>

	</main>

<?php include('footer.php'); ?>
