<?php $bodyclass = 'default-page not-home'; ?>
<?php include('header.php'); ?>

	<!-- BG SVG -->
	<div class="top-bg-about">
		<div class="container">
			<div class="the_bg">
			</div>
		</div>
	</div>
	<!-- BG SVG -->

	<main class="main" role="main">
		<section class="section page-top-sec">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">

						<div class="top-label w-blue grey">
							<span>Última atualização: 15/05/2018.</span>
						</div>

						<h1 class="title">
							Nossas políticas<br/>de privacidade
						</h1>

					</div>
				</div>
			</div>
		</section>

		<section class="main-content">
			<div class="container">
				<div class="row">
					
					<article class="article-content col-xs-12 col-lg-8">
						<section id="politica-privacidade">
							<div class="sec-title anchored-item" id="politicas">
								<h2 class="title">POLÍTICA DE PRIVACIDADE</h2>
							</div>
							<p>A <strong>MODIAX Ltda.</strong>, proprietária do site MODIAX, é uma sociedade empresarial limitada com sede e domicílio na XXXXXXXXXXXXX, na cidade de São Paulo, SP, Cep: XXXXX-XXX. Inscrita no CNPJ sob o nº XX.XXX.XXX/XXXX-XX, a MODIAX é uma plataforma eletrônica desenvolvida com a finalidade de intermediar a compra e venda de criptomoedas, tais como Bitcoin.</p>
							<p>A <strong>MODIAX</strong>, através de seu site e de seu aplicativo móvel, coletará dados e informações pessoais, visando, sobretudo, identificar seus usuários e proporcionar uma experiência segura, sempre protegendo as informações disponibilizadas e respeitando a sua privacidade.</p>
							<p>A <strong>MODIAX</strong> reconhece que a sua privacidade é muito importante, portanto tomamos todas as medidas possíveis para protegê-la. Nesse sentido, a presente Política de Privacidade (“Política”) visa lhe informar como as suas informações e dados serão coletados, usados, compartilhados e armazenados por meio dos nossos sistemas e do nosso aplicativo. Este documento deve ser lido e interpretado em conjunto com os nossos Termos de Uso.</p>
							<p>A aceitação da nossa Política será feita quando você se cadastrar em nossa plataforma para o uso do site <strong>MODIAX</strong>. Isso indicará que você está ciente e em total acordo com a forma como utilizaremos as suas informações e seus dados.</p>
							<p>Caso não concorde com esta Política, por favor, não continue o seu procedimento de registro e não utilize nossos serviços.</p>
							<p>A presente Política está dividida da seguinte forma para facilitar a sua compreensão:</p>
							<ul>
								<li>1. Quais informações e dados a <strong>MODIAX</strong> coleta;</li>
								<li>2. Como a <strong>MODIAX</strong> usa os dados e informações coletadas;</li>
								<li>3. Como, quando e com quem a <strong>MODIAX</strong> compartilha suas informações;</li>
								<li>4. Como a <strong>MODIAX</strong> protege as suas informações e seus dados;</li>
								<li>5. Atualizações dessa política de privacidade;</li>
								<li>6. Lei aplicável.</li>
							</ul>
							<br/>
							<p>Caso haja alguma dúvida sobre a forma como tentamos proteger a sua privacidade, por favor, entre em contato conosco através do e-mail XXXXXX@XXXXXXXX.XXXX.</p>

							<div class="sec-title anchored-item" id="info">
								<h2 class="title">INFORMAÇÕES QUE COLETAMOS</h2>
							</div>
							<div class="numbered">
								<p><span class="number">01</span>Dados de cadastro: no momento em que quiser iniciar seu uso do site <strong>MODIAX</strong>, você terá que fazer um cadastro para criar a sua conta conosco. Para se cadastrar, será necessário nos fornecer o seu CPF, data de nascimento e e-mail. Caso você represente uma pessoa jurídica, você deverá nos fornecer o CNPJ da pessoa jurídica, sua data de constituição e um e-mail para contato. Com esses dados, você poderá realizar transações até o limite definido em nossos Termos de Uso.</p>
								<p>Caso deseje realizar transações acima dos limites básicos definidos nos nossos Termos de Uso, você também deverá nos enviar fotos de sua Carteira Nacional de Habilitação (CNH) ou Carteira de Identidade (RG), conjuntamente com a foto de um comprovante de residência.</p>
								<p><span class="number">02</span>Checagem de dados para cadastro: a <strong>MODIAX</strong> reserva-se o direito de verificar a veracidade e outros aspectos relacionados às suas informações cadastrais, podendo negar o seu cadastro, a seu único e exclusivo critério, com base nas informações coletadas. Essa checagem poderá ser realizada por outras empresas, contratadas especificamente para estes fins. Para isso, o seu número de CPF ou de CNPJ poderão ser compartilhados com tais empresas. Entre outras informações, nos reservamos o direito de verificar, perante órgãos do poder público, como tribunais de justiça e tribunais federais, a sua participação em processos judiciais de qualquer natureza, seu status perante instituições de proteção ao crédito, listas nacionais e internacionais, como as listas emitidas pela Interpol e a Specially Designated Nationals and Blocked Persons List ou listas similares, assim como outras informações publicamente acessíveis sobre você.</p>
								<p>Além disso, para maior segurança, será solicitado que você realize um processo de autenticação dupla por meio de ferramentas de envio de tokens (chaves de segurança), tais como o Google Authenticator ou o Authy.</p>
								<p><span class="number">03</span>Dados de pagamento: a <strong>MODIAX</strong> permite que você compre e venda criptomoedas. Para tanto, serão coletados dados relativos às transações que você realizar. Sempre que uma transação ocorrer, nosso sistema armazenará a data, o valor e os usuários que participaram da transação.</p>
								<p><span class="number">04</span>Dados coletados automaticamente: a <strong>MODIAX</strong> coleta alguns dados automaticamente, incluindo as características do seu aparelho, dentre as quais o seu sistema operacional, o idioma, além de registros de acesso à aplicação, que inclui o endereço IP, com data e hora de acesso à <strong>MODIAX</strong>. Alguns desses dados são de coleta obrigatória, de acordo com a Lei 12.965/2014, mas somente serão fornecidos para terceiros com a sua autorização expressa ou por meio de demanda judicial. Conforme mencionado acima, para facilitar a sua identificação, a <strong>MODIAX</strong> usa uma chave de segurança, que servirá para autenticá-lo e manter as suas configurações salvas. Essa chave de segurança será armazenada em um cookie, que permitirá à <strong>MODIAX</strong>, para sua segurança, o controle da duração da sua sessão.</p>
							</div>

							<div class="sec-title anchored-item" id="dados">
								<h2 class="title">USO DOS DADOS</h2>
							</div>
							<div class="numbered">
								<p><span class="number">01</span>Todos os dados coletados serão excluídos de nossos servidores quando você assim requisitar ou quando estes não forem mais necessários ou relevantes para lhe oferecermos os nossos serviços, salvo se houver qualquer outra razão para a sua manutenção, como eventual obrigação legal de retenção de dados ou necessidade de preservação destes para resguardo de direitos da <strong>MODIAX</strong>.</p>
								<p><span class="number">02</span>O <strong>MODIAX</strong> reserva-se o direito de monitorar toda a plataforma, incluindo a frequência e partes das transações em que você fizer parte, o número de sessões ativas, sistemas e dispositivos que acessam a plataforma, entre outros, principalmente para assegurar que a <strong>MODIAX</strong> não será utilizada para fins ilegítimos e para assegurar que as regras descritas em nossos Termos de Uso estão sendo observadas. Portanto, a <strong>MODIAX</strong> se reserva o direito de bloquear a conta de determinado usuário, a seu único e exclusivo critério, se tal monitoramento indicar que a plataforma pode estar sendo usada para tais fins, sendo possível a comprovação da legitimidade da transação caso o usuário o prove por meio do reenvio das informações previstas no item 1 acima. A <strong>MODIAX</strong> também se reserva o direito de excluir determinado usuário, independente do tipo que for, caso a presente Política não seja respeitada.</p>
							</div>

							<div class="sec-title anchored-item" id="compartilhamento">
								<h2 class="title">COMPARTILHAMENTO DE DADOS</h2>
							</div>
							<div class="numbered">
								<p><span class="number">01</span>A <strong>MODIAX</strong> reserva-se o direito de compartilhar suas informações com parceiros comerciais com a única finalidade de verificar a veracidade e seu status legal, conforme mencionado no item 1 acima. Por meio deste documento, você autoriza expressamente tais compartilhamentos.</p>
								<p><span class="number">02</span>Todos os dados, informações e conteúdos sobre você podem ser considerados ativos no caso de negociações em que a <strong>MODIAX</strong> fizer parte. Portanto, nos reservamos o direito de incluir seus dados dentre os ativos da empresa caso esta venha a ser vendida, adquirida ou fundida com outra. Por meio desta Política você concorda e está ciente desta possibilidade.</p>
								<p><span class="number">03</span>Ainda, a <strong>MODIAX</strong> se reserva o direito de fornecer seus dados e informações pessoais, incluindo suas interações, caso seja requisitado judicialmente para tanto, ato necessário para que a empresa esteja em conformidade com as leis nacionais.</p>
							</div>

							<div class="sec-title anchored-item" id="seguranca">
								<h2 class="title">SEGURANÇA DOS DADOS</h2>
							</div>
							<div class="numbered">
								<p><span class="number">01</span>Todos os seus dados são confidenciais e somente as pessoas com as devidas autorizações terão acesso a elas. O seu nome, CPF ou CNPJ, data de nascimento ou data de constituição e e-mail poderão ser acessados pelo setor de atendimento ao cliente do <strong>MODIAX</strong> para possibilitar tal atendimento.</p>
								<p><span class="number">02</span>Qualquer uso de seus dados estará de acordo com a presente Política. A <strong>MODIAX</strong> empreenderá todos os esforços para garantir a segurança dos nossos sistemas e dos seus dados. Nossos servidores estão localizados em diferentes locais para garantir estabilidade e segurança, e somente podem ser acessados por meio de canais de comunicação previamente autorizados.</p>
								<p><span class="number">03</span>Todos os seus dados serão sempre armazenados de forma criptografada. A qualquer momento você poderá requisitar cópia dos seus dados armazenados em nossos sistemas, basta entrar em contato conosco e fazer um pedido. Manteremos os dados e informações somente até quando estas forem necessárias ou relevantes para as finalidades descritas nesta Política, ou em caso de períodos pré-determinados por lei, ou até quando estas forem necessárias para a manutenção de interesses legítimos da <strong>MODIAX</strong>.</p>
								<p><span class="number">04</span>A <strong>MODIAX</strong> considera a sua privacidade e a proteção dos seus dados extremamente importante. Faremos o que estiver ao nosso alcance para proteger suas informações, todavia, não temos como garantir complemente que todos os dados e informações sobre você em nossa plataforma estarão livres de acessos não autorizados, principalmente caso haja compartilhamento indevido das credenciais necessárias para acessar o nosso aplicativo. Portanto, você é o único responsável por manter sua senha de acesso em local seguro e é vedado o compartilhamento desta com terceiros. Você se compromete a notificar a <strong>MODIAX</strong> imediatamente, através de meio seguro, a respeito de qualquer uso não autorizado de sua conta, bem como o acesso não autorizado por terceiros a esta.</p>
							</div>

							<div class="sec-title anchored-item" id="atualizacoes">
								<h2 class="title">ATUALIZAÇÕES DA POLÍTICA DE PRIVACIDADE</h2>
							</div>
							<div class="numbered">
								<p><span class="number">01</span>A <strong>MODIAX</strong> se reserva o direito de alterar essa Política quantas vezes forem necessárias, visando fornecer aos seus clientes cada vez mais segurança e conveniência. É por isso que é muito importante acessar nossa Política periodicamente. Para facilitar, sempre indicaremos no fim do documento a data da última atualização. Caso sejam feitas alterações relevantes que ensejem novas autorizações de nossos clientes, enviaremos uma notificação para cada um de nossos usuários.</p>
							</div>

							<div class="sec-title anchored-item" id="lei">
								<h2 class="title">LEI APLICÁVEL</h2>
							</div>
							<div class="numbered">
								<p><span class="number">01</span>Fica eleito o foro da Comarca de XXXXXXX, Estado de XXXXXXX, como o único competente para anular quaisquer controvérsias decorrentes deste documento, independentemente de qualquer outro por mais privilegiado que seja ou venha a ser.</p>
							</div>

						</section>
					</article>

					<aside class="sidebar hidden-xs hidden-sm col-lg-4">
						<div class="sidebar">
							<ul>
								<li class="scroll-anchor"><a href="#politicas">POLÍTICA DE PRIVACIDADE</a></li>
								<li class="scroll-anchor"><a href="#info">INFORMAÇÕES QUE COLETAMOS</a></li>
								<li class="scroll-anchor"><a href="#dados">USO DOS DADOS</a></li>
								<li class="scroll-anchor"><a href="#compartilhamento">COMPARTILHAMENTO DE DADOS</a></li>
								<li class="scroll-anchor"><a href="#seguranca">SEGURANÇA DOS DADOS</a></li>
								<li class="scroll-anchor"><a href="#atualizacoes">ATUALIZAÇÕES DA POLÍTICA DE PRIVACIDADE</a></li>
								<li class="scroll-anchor"><a href="#lei">LEI APLICÁVEL</a></li>
							</ul>
						</div>
					</aside>

				</div>
			</div>
		</section>


		<?php include('includes/account-boxes.php'); ?>

	</main>

<?php include('footer.php'); ?>
