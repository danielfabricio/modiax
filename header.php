<!doctype html>
<html lang="pt-BR" prefix="og: http://ogp.me/ns#">
	<head>
		<meta charset="UTF-8">
		<title>Mondiax</title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="images/icons/favicon.png" rel="shortcut icon">
        <link href="images/icons/favicon_200.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Lorem ipsum dollor">

		<link rel="canonical" href="site_url_here" />
		<meta property="og:locale" content="pt_BR" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Lorem ipsum" />
		<meta property="og:url" content="site_url_here" />
		<meta property="og:site_name" content="Mondiax" />
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:title" content="Lorem ipsum" />

		<link rel='stylesheet' href='css/home.css' media='all' />
	</head>
	
	<body class="body default-m-top <?php echo $bodyclass; ?>">
		<!-- header -->
		<header class="main-header" role="banner" id="main-header">
			

			<div class="container">
				<div class="row">
					<!-- logo -->
					<div class="col-xs-6 col-sm-4 col-lg-2">
						<a href="#" class="logo">
							<svg width="120" height="21" viewBox="0 0 120 21" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" clip-rule="evenodd" d="M26.2742 1.64041e-05H22.1079C21.9378 1.64041e-05 21.7764 0.0744488 21.6665 0.203416L13.1132 10.2327C13.0135 10.3542 12.8639 10.4247 12.7061 10.4247C12.5481 10.4247 12.3986 10.3542 12.2989 10.2327L6.68765 3.69515C6.58706 3.57524 6.43765 3.50636 6.28054 3.5074H3.07559C2.91702 3.50621 2.7665 3.57616 2.6658 3.69774L0.0677734 6.79573C-0.00241706 6.87285 -0.019751 6.98393 0.0235839 7.07851C0.0669189 7.17308 0.162744 7.23308 0.267358 7.23122H2.55288H4.81204L12.1675 16.2383C12.2682 16.3616 12.4201 16.4326 12.58 16.4313H12.819C12.9776 16.4325 13.1281 16.3625 13.2288 16.2409L26.5001 0.469407C26.5717 0.383317 26.5865 0.263871 26.538 0.163163C26.4895 0.0624859 26.3866 -0.00117378 26.2742 1.64041e-05ZM45.7931 12.1546C45.7931 7.0591 41.9656 3.25704 36.8325 3.25704C31.752 3.25704 27.9219 7.0591 27.9219 12.1546C27.9219 17.1979 31.752 21 36.8325 21C41.9629 21 45.7931 17.1979 45.7931 12.1546ZM108.097 11.8025L102.667 3.92461C102.607 3.8441 102.599 3.73702 102.646 3.64879C102.693 3.56059 102.787 3.5067 102.887 3.51H107.025C107.202 3.50856 107.368 3.59581 107.466 3.74208L110.857 8.67067C110.954 8.82057 111.121 8.91127 111.3 8.91127C111.479 8.91127 111.646 8.82057 111.743 8.67067L115.124 3.74208C115.222 3.59581 115.388 3.50856 115.565 3.51H119.729C119.829 3.5067 119.923 3.56059 119.97 3.64879C120.017 3.73702 120.009 3.8441 119.949 3.92461L114.535 11.8025C114.409 11.9819 114.409 12.2204 114.535 12.3997L119.944 20.3402C120.003 20.4208 120.011 20.5278 119.964 20.616C119.917 20.7043 119.824 20.7582 119.723 20.7549H115.607C115.43 20.7563 115.264 20.6691 115.166 20.5228L111.732 15.5081C111.636 15.3582 111.469 15.2675 111.29 15.2675C111.11 15.2675 110.944 15.3582 110.847 15.5081L107.416 20.5123C107.318 20.6586 107.152 20.7459 106.975 20.7444H102.885C102.784 20.7477 102.691 20.6938 102.644 20.6056C102.597 20.5174 102.605 20.4103 102.664 20.3298L108.099 12.4023C108.226 12.2218 108.225 11.9819 108.097 11.8025ZM92.2061 3.50997H87.9215C87.6995 3.50557 87.4986 3.64024 87.4198 3.84639L80.9785 20.3819C80.9436 20.4637 80.953 20.5576 81.0034 20.6309C81.054 20.7042 81.1386 20.747 81.2281 20.7444H84.8086C85.0327 20.7477 85.2342 20.6094 85.3103 20.4002L86.0537 18.4366C86.2495 17.9193 86.7476 17.5766 87.3042 17.576H92.7315C93.2898 17.5793 93.7874 17.9266 93.9793 18.447L94.7017 20.3976C94.777 20.6079 94.9787 20.7473 95.2035 20.7444H98.7735C98.8645 20.7513 98.9527 20.7108 99.0062 20.6374C99.0598 20.564 99.0712 20.4683 99.0362 20.3846L92.7052 3.85158C92.6284 3.6443 92.4286 3.50746 92.2061 3.50997ZM73.2342 3.51H76.4442C76.7344 3.51 76.9696 3.74349 76.9696 4.03154V20.1994C76.9696 20.4875 76.7344 20.721 76.4442 20.721H73.2342C72.944 20.721 72.7088 20.4875 72.7088 20.1994V4.03154C72.7088 3.8932 72.7641 3.76058 72.8627 3.66277C72.9612 3.56496 73.0949 3.51 73.2342 3.51ZM50.8736 4.03935C50.8736 3.7513 51.1088 3.51781 51.3989 3.51781H58.6598C63.7929 3.54387 67.6204 6.55843 67.6204 12.1598C67.6204 17.7117 63.7903 20.7522 58.6598 20.7522H51.4069C51.1167 20.7522 50.8815 20.5188 50.8815 20.2307L50.8736 4.03935ZM18.6186 13.0321V20.3989C18.6186 20.6869 18.8538 20.9205 19.1439 20.9205H22.3725C22.6626 20.9205 22.8979 20.6869 22.8979 20.3989V7.92095L18.6186 13.0321ZM3.1918 8.97707H4.36599L7.46328 12.7948V20.2633C7.46328 20.5513 7.22805 20.7848 6.93789 20.7848H3.71719C3.42703 20.7848 3.1918 20.5513 3.1918 20.2633V8.97707ZM55.166 16.2461V8.00569C55.1625 7.79475 55.2436 7.59105 55.3914 7.43947C55.5392 7.28786 55.7416 7.20073 55.9541 7.19731H58.4707C61.2842 7.19731 63.1992 8.95754 63.1992 12.152C63.1992 15.2943 61.279 17.0441 58.4707 17.0441H55.9541C55.5158 17.0369 55.1646 16.6814 55.166 16.2461ZM88.234 14.4624H91.8173C91.9938 14.467 92.1609 14.3832 92.262 14.2393C92.3629 14.0954 92.3844 13.9108 92.319 13.7479L90.3278 8.37862C90.2924 8.27159 90.1917 8.19927 90.0781 8.19927C89.9647 8.19927 89.864 8.27159 89.8286 8.37862L87.7271 13.7427C87.6603 13.9063 87.6814 14.0924 87.7832 14.2371C87.885 14.3819 88.0537 14.4657 88.2315 14.4598L88.234 14.4624ZM41.5873 12.1546C41.5873 14.8901 39.667 16.9945 36.8588 16.9945C34.1267 16.9945 32.1355 14.8901 32.1303 12.1546C32.1303 9.37474 34.1267 7.28856 36.8588 7.28856C39.6723 7.28856 41.5873 9.36693 41.5873 12.1546Z" fill="#265CFF"/>
							</svg>

						</a>
					</div>
					<!-- /logo -->

					<!-- nav -->
					<div class="col-xs-6 col-sm-8 col-lg-10">
						<nav class="main-nav dd-non-style-list" role="navigation">
							<button class="hamburger hamburger--slider nav-trigger" type="button">
								<span class="hamburger-box">
									<span class="hamburger-inner"></span>
								</span>
							</button>

							<div class="menu-list">
								<ul class="non-styled-list main-list">
									<li><a href="" class="primary">Para traders</a></li>
									<li class="hidden-sm"><a href="" class="primary">Sobre nós</a></li>
									<li class="hidden-xs"><a href="" class="primary">Entrar</a></li>
									<li class="hidden-sm hidden-lg"><a href="" class="primary">Dúvidas Frequentes</a></li>
									<li class="hidden-sm hidden-lg"><a href="" class="sub">Política de Privacidade</a></li>
									<li class="hidden-sm hidden-lg"><a href="" class="sub">Termos de uso</a></li>
									<li class="hidden-sm hidden-lg"><a href="" class="sub">Segurança</a></li>
									<li class="hidden-sm hidden-lg"><a href="" class="sub">Carreiras</a></li>
									<li class="hidden-sm hidden-lg"><a href="" class="sub">Chat</a></li>
									<li class="hidden-sm hidden-lg"><a href="" class="sub">Taxas, limites e prazos</a></li>
									<li class="hidden-xs menu-btn"><a href="" class="btn white h-ghost medium">Criar conta</a></li>
								</ul>
								<div class="mobile-login hidden-sm hidden-lg">Já é cadastrado? <a href="#">entrar</a></div>
								<div class="menu-btn hidden-sm hidden-lg"><a href="" class="btn white medium">Criar conta</a></div>
							</div>
						</nav>
					</div>
				</div>
				<!-- /nav -->
			</div>


		</header>
		<!-- /header -->
