<?php $bodyclass = 'home' ?>
<?php include('header.php'); ?>

	<!-- BG SVG -->
	<div class="top-bg">
		<div class="container">
			<div class="the_bg">
				<img src="images/shape-bg.svg" class="bg-1">
				<img src="images/line-bg.svg" class="bg-2">
			</div>
		</div>
	</div>
	<!-- BG SVG -->

	<main class="main" role="main">
		<section class="section top-sec">
			<div class="container">
				<div class="row">
					<div class="top-desc col-xs-12 col-sm-6 col-lg-5">
						<div class="top-label grey">
							<span>Do seu jeito!</span>
						</div>
						<h1 class="title">Simples e segura, a sua plataforma para <strong>comprar e vender bitcoins</strong></h1>
					</div>

					<div class="top-simulator col-xs-12 col-sm-6 col-lg-5 col-lg-push-2">
						<?php include('includes/simulador.php'); ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-5 email-form">
						<form action="">
							<input type="text" class="input" placeholder="Insira seu email">
							<button class="btn rounded small blue">Comece a investir <i class="icon-right-arrow"></i></button>
						</form>
					</div>
				</div>
			</div>
		</section>

		<?php include('includes/como-comprar.php') ?>
		<?php include('includes/grafico.php') ?>
		<?php include('includes/simulador_bottom.php') ?>
		<?php include('includes/text-features.php') ?>
		<?php include('includes/faq-sec.php') ?>
	</main>

<?php include('footer.php'); ?>
