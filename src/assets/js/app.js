/**
 * @description
 * App js
 */

var mask = function(){
	$('.money-mask').mask('000.000.000.000.000,00', {reverse: true});
	$('.bitcoin-mask').mask('9999999999.9999999999');
	$('.year-mask').mask('9999');
	$('.number-mask').mask('99999');


	$.fn.textWidth = function(text, font) {
	    if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
	    $.fn.textWidth.fakeEl.text(text || this.val() || this.text() || this.attr('placeholder')).css('font', font || this.css('font'));
	    return $.fn.textWidth.fakeEl.width();
	};

	$('.width-dynamic').on('input', function() {
	    var inputWidth = $(this).textWidth();
	    $(this).css({
	        width: inputWidth
	    })
	}).trigger('input');


	function inputWidth(elem, minW, maxW) {
	    elem = $(this);
	}

	var targetElem = $('.width-dynamic');

	inputWidth(targetElem);
}

var menu = function(){
    $('.nav-trigger').click(function(){
    	var wscroll = $(window).scrollTop();
    	$(this).toggleClass('is-active');
    	$('.menu-list').toggleClass('active');
    	$('body').toggleClass('overflow');
    	if (wscroll < 201) {
    		$('.main-header').toggleClass('fixed');
    	}
    });

    function fixMenu(){
    	var wscroll = $(window).scrollTop();
    	if (wscroll > 200) {
    		$('.main-header').addClass('fixed');
    	}else{
    		$('.main-header').removeClass('fixed');
    	}
    }
    fixMenu();
    $(window).scroll(function(){
    	fixMenu();
    });

    // HIDE IF SCROLL TO BOTTOM
    var newscroll = $(window).scrollTop();

    $(window).scroll(function(){
    	var lastscroll = newscroll;
    	newscroll = $(window).scrollTop();
    	if (newscroll > lastscroll) {
    		$('.main-header').addClass('to-bottom');
    	}else{
    		$('.main-header').removeClass('to-bottom');
    	}
    });
}

var simulador = function(){

	// SIMULADOR VALS
	var compraVal = '';
	var vendaVal = '';
	var matchVal = '';
	$.ajax({
        type: "GET",
        dataType: "JSON",
        url: 'example.html',
        success: function(data){
            compraVal = data.btcbrl.ticker.buy;
			vendaVal = data.btcbrl.ticker.sell;
			matchVal = data.btcbrl.ticker.last;
			calcVal();
        }
    });


	$('.top-simulator .type-item').click(function(){
		$('.top-simulator .type-item').removeClass('active');
		$(this).addClass('active');

		if ( $(this).attr('data-type') == 'comprar') {
			$('.comprar-tab').show();
			$('.comprar-tab').addClass('active');
			$('.vender-tab').hide();
			$('.vender-tab').removeClass('active');
		}else{
			$('.comprar-tab').hide();
			$('.comprar-tab').removeClass('Active');
			$('.vender-tab').show();
			$('.vender-tab').addClass('active');
		}
	});

	$('.simulador-text .text-input.ano').click(function(){
		$('.dropdown', this).slideToggle(300);
	});
	$('.simulador-text .text-input.ano .item').click(function(){
		$('.year-value').html($(this).html());
	});

	// CALCULO

	function formatNumber(n, currency) {
	    return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	}

	function calcVal(){
		// valor condicional
		var money = parseInt($('#money_val').val());
		var year = $('.year-value').html();
		var cotacao = matchVal;
		var valor_bitcoin = '';
		console.log(matchVal);

		if(year == '2010'){
			valor_bitcoin = 0.5022;
		}
		else if(year == '2011'){
			valor_bitcoin = 0.5022;
		} 
		else if(year == '2012'){
			valor_bitcoin = 8.31885;
		} 
		else if(year == '2013'){
			valor_bitcoin = 26.8772;
		} 
		else if(year == '2014'){
			valor_bitcoin = 1811.43954;
		} 
		else if(year == '2015'){
			valor_bitcoin = 834.47754;
		} 
		else if(year == '2016'){
			valor_bitcoin = 1755.37932;
		} 
		else if(year == '2017'){
			valor_bitcoin = 3190.43896;
		} 
		else if(year == '2018'){
			valor_bitcoin = 43213.02;
		}

		var qtd = (money / valor_bitcoin) * cotacao;
		$('.simulador-text .result').html(formatNumber(qtd, 'R$'));
	}

	calcVal();
	$('#money_val').keyup(function(){
		calcVal();
	});
	$('#money_val').blur(function(){
		calcVal();
	});
	$('.text-input.ano').click(function(){
		setTimeout(calcVal(), 300);
	});



	// CALCULOS
	$('.simulador-input input').keyup(function(){
		var input = $(this);
		var thisVal = $(this).val();
		if (input.hasClass('money-mask')) {
		    thisVal = $(this).val().replace(/\./g,'');
		    thisVal = thisVal.replace(',','.');
		}
		var result = '';

		console.log(thisVal);

		if (input.parent().parent().hasClass('comprar-tab')) {
			if (input.hasClass('money-mask')) {
				result = thisVal/compraVal;
				$('.comprar-tab input.bitcoin-mask').val('');
				$('.comprar-tab input.bitcoin-mask').val(result);
			}else{
				result = thisVal*compraVal;
				$('.comprar-tab input.money-mask').val('');
				$('.comprar-tab input.money-mask').val(formatNumber(result, ''));
			}
		}else{
			if (input.hasClass('money-mask')) {
				result = thisVal/vendaVal;
				$('.vender-tab input.bitcoin-mask').val('');
				$('.vender-tab input.bitcoin-mask').val(result);
			}else{
				result = thisVal*vendaVal;
				$('.vender-tab input.money-mask').val('');
				$('.vender-tab input.money-mask').val(formatNumber(result, ''));
			}
		}
	});

}

var grafico = function(){
	$(document).on('click', '.chart-bullet', function(){
		var thisEl = $(this);

		$('.chart-bullet .label').removeClass('active');
		$('.chart-bullet .line').attr('style', '');
		$('.chart-bullet .label-content').attr('style', '');

		$('.label', this).addClass('active');
		$( ".line", this ).animate({
		    height: '100px'
		}, 300, function() {
		    $( ".label-content", thisEl ).animate({
			    width: '200px'
			}, 300)
		});
	});
}

var accordeon = function(){
	$('.accordion-item .item-button').click(function(){
		var wSize = $('body').width();
		var accItem = $(this).parent();

		if (!$(accItem).hasClass('active')) {
			$('.accordion-item').removeClass('active');
			$('.more-to-arrow').removeClass('active');
			if (wSize > 1199) {
				$('.accordion-item .item-content').fadeOut();
			}else{
				$('.accordion-item .item-content').slideUp(300);
			}

			$(accItem).addClass('active');
			if (wSize > 1199) {
				$('.item-content', accItem).addClass('active');
				$('.more-to-arrow', accItem).addClass('active');
				$('.item-content', accItem).fadeIn();
			}else{
				$('.item-content', accItem).addClass('active');
				$('.more-to-arrow', accItem).addClass('active');
				$('.item-content', accItem).slideDown(300);
			}
		}
	});
}

var scrollToAnchor = function(){
	$('.scroll-anchor a').click(function(e) {
	    $('.scroll-anchor').removeClass('active');
	    $(this).parent().addClass('active');

		e.preventDefault();
	    var the_id = $(this).attr('href');
	    var target = $(the_id);
	    if (target.length) {
	    	var targetoffset = target.offset().top;
	    	if (the_id == '#main-header') {
	    		targetoffset = 0;
	    	}
	    	console.log(targetoffset);
	        $('html,body').animate({
	            scrollTop: targetoffset
	        }, 1000);
	        return false;
	    }
	});

	// TOGGLE ON SCROLL
	function toggle_anchor_on_scroll(){

		$('.anchored-item').each(function(){
			var wScroll = $(window).scrollTop();

			if ($(this).offset().top < wScroll + $(window).height() - 50 && $(this).offset().top + $(this).height() > wScroll - $(window).height() + 50) {
				var theID = $(this).attr('id');
				
				$('.sidebar .scroll-anchor').removeClass('active');
				$('.sidebar a[href="#'+theID+'"]').parent().addClass('active');
	        }
			
		});
	}

	toggle_anchor_on_scroll();
	$(window).scroll(function(){
		toggle_anchor_on_scroll();
	});
}

var sidebarScroll = function(){
	if ($('aside .sidebar').length) {
		function scroll(){
			var wOffset = $(window).scrollTop();
			var startScroll = $('.main-content').offset().top - 100;
			var endScroll = $('.main-content').offset().top;
				endScroll = endScroll + $('.main-content').height();
				endScroll = endScroll - $('.sidebar').height();
				endScroll = endScroll - 300;

			// console.log('tela:'.wOffset.'Start:'.startScroll.'end:'.endScroll);

			if (wOffset > startScroll && wOffset < endScroll) {
				$('aside .sidebar').css(
					'top', $(window).scrollTop() + 100 - $('.main-content').offset().top
				);
				$('aside .sidebar').addClass('absolute');
			}else if(wOffset >= endScroll){
				$('aside .sidebar').css(
					'bottom', 0
				);
			}else{
				$('aside .sidebar').removeClass('absolute');
			}
		}

		scroll();
		$(window).scroll(function(){
			scroll();
		});
	}
}



var ComoComprar = function(){
	function comocomprarScroll(){
		var wScroll = $(window).scrollTop();
		var doClass = $('.como-comprar').offset().top;
		    doClass = doClass - 600;

		if (wScroll > doClass) {
			$('.como-comprar .arrow-animate').addClass('active');
		}
	}

	comocomprarScroll();
	$(window).scroll(function(){
		comocomprarScroll();
	});
}

$(document).ready(function () {
    menu();
    mask();
    simulador();
    grafico();
    accordeon();
    scrollToAnchor();
    sidebarScroll();
    ComoComprar();
});

$(window).load(function(){
});



