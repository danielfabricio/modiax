var gulp = require('gulp');
var path = 'assets';
var npm = 'node_modules';
var tinypng = require('gulp-tinypng-compress');

// ESTILO
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var cleanCSS = require('gulp-clean-css');

// SCRIPT
var uglify = require('gulp-uglify');
var pump = require('pump');
var concat = require('gulp-concat');

// VERSION
var bust = require('gulp-buster');



// FUNCOES DE ESTILO
gulp.task('version', function () {
  return gulp.src([
    'assets/sass/*.scss',
    'assets/js/*.js'
    ])

    .pipe(bust())
    .pipe(gulp.dest('../css'));
});

gulp.task('sass', function () {
  return gulp.src([
    'assets/sass/*.scss'
    ])

    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('../css'));
});

gulp.task('prod', function () {
  return gulp.src([
    'assets/sass/*.scss'
    ])

    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('../css'));
});


// FUNCOES DE JAVASCRIPT
gulp.task('vendor', function() {
  return gulp.src([
       npm + '/jquery/dist/jquery.js',
       npm + '/masonry-layout/dist/masonry.pkgd.js',
       npm + '/jquery-mask-plugin/dist/jquery.mask.js',
     ])
    .pipe(concat('vendor.js'))
    .pipe(uglify({
        mangle: false
    }))
    .pipe(gulp.dest('../js/'));
});


gulp.task('scripts', function () {
    return gulp.src([
        'assets/js/*.js',
       npm + '/swiper/dist/js/swiper.js',
       npm + '/jquery-mask-plugin/dist/jquery.mask.js',
       npm + '/lightbox2/src/js/lightbox.js'
    ])
    // .pipe(concat('app.js'))
    .pipe(uglify({
        mangle: false
    }))
    .pipe(gulp.dest('../js'));
});

 
gulp.task('default', ['vendor', 'scripts', 'sass', 'version']);

gulp.task('watch', function () {
  gulp.watch('assets/sass/**/*.scss', ['sass', 'version']);
  gulp.watch('assets/js/**/*.js', ['scripts', 'version']);
});

gulp.task('ampwatch', function () {
  gulp.watch('assets/sass/**/*.scss', ['amp']);
});

