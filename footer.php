
		<!-- footer -->
		<footer class="main-footer" role="contentinfo">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-lg-4 footer-info">
						<div class="logo">
							<img src="images/modiax-branco.svg" alt="">
							<div class="redes">
								<a href="https://facebook.com" target="_blank"><i class="icon-facebook"></i></a>
								<a href="https://twitter.com" target="_blank"><i class="icon-twitter"></i></a>
							</div>
						</div>
						<div class="copyright">
							<p class="hidden-lg">MDX Tech LTDA   –   CNPJ: 29.642.632/0001-30 <span>Todos direitos reservados.</span></p>
							<p class="hidden-xs hidden-sm">MDX Tech LTDA<br/>CNPJ: 29.642.632/0001-30<br/>Todos direitos reservados.</p>
						</div>
					</div>

					<div class="col-xs-12 col-sm-11 col-sm-push-1 col-lg-push-0 col-lg-8 footer-menu">
						<div class="menu-block clear">
							<div class="menu-title">
								A Empresa
							</div>
							<ul class="non-styled-list">
								<li> <a href="">Exchange</a></li>
								<li> <a href="">Sobre a Modiax</a></li>
								<li> <a href="">Segurança</a></li>
								<li> <a href="">Carreiras</a></li>
							</ul>
						</div>

						<div class="menu-block clear">
							<div class="menu-title">
								Legal
							</div>
							<ul class="non-styled-list">
								<li> <a href="">Política de Privacidade</a></li>
								<li> <a href="">Termos de uso</a></li>
							</ul>
						</div>

						<div class="menu-block clear">
							<div class="menu-title">
								Ajuda
							</div>
							<ul class="non-styled-list">
								<li> <a href="">FAQ</a></li>
								<li> <a href="">Chat</a></li>
								<li> <a href="">Taxas, limites e prazos</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<div class="scroll-to-top hidden-sm hidden-lg scroll-anchor">
			<a href="#main-header">
				<img src="images/bt-bacttotop.svg">
			</a>
		</div>
		<!-- /footer -->
		<script type='text/javascript' src='js/vendor.js'></script>
		<script type='text/javascript' src='js/app.js'></script>
	</body>
</html>
