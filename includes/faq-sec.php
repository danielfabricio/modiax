<section class="section faq-sec">
	<div class="container">

		<div class="faq-bg"></div>
		<div class="row">
			<div class="block-header col-xs-12">
				<h3 class="h2 blue">Dúvidas frequentes</h3>
			</div>

			<div class="accordion-block col-xs-12 col-lg-6">

				<!-- item -->
				<div class="accordion-item active">
					<div class="item-button">
						<div class="label">O que é bitcoin?</div>
						<div class="button">
							<div class="more-to-arrow active">
								<div class="shape-1"></div>
								<div class="shape-2"></div>
							</div>
						</div>
					</div>

					<div class="item-content active" style="display: block;">
						<div class="content-title h3 dark-blue hidden-xs hidden-sm">O que é bitcoin?</div>
						<div class="item-text">
							<p>A maneira mais fácil de comprar bitcoins é através de corretoras, como a Modiax. Você abre a sua conta, transfere reais e troca por Bitcoins. Simples e seguro!</p>
							<a href="" class="btn orange square-w">Comprar bitcoins</a>
						</div>
					</div>
				</div>
				<!-- //item -->

				<!-- item -->
				<div class="accordion-item">
					<div class="item-button">
						<div class="label">Como comprar bitcoin?</div>
						<div class="button">
							<div class="more-to-arrow">
								<div class="shape-1"></div>
								<div class="shape-2"></div>
							</div>
						</div>
					</div>

					<div class="item-content">
						<div class="content-title h3 dark-blue hidden-xs hidden-sm">Como comprar bitcoin?</div>
						<div class="item-text">
							<p>A maneira mais fácil de comprar bitcoins é através de corretoras, como a Modiax. Você abre a sua conta, transfere reais e troca por Bitcoins. Simples e seguro!</p>
							<a href="" class="btn orange square-w">Comprar bitcoins</a>
						</div>
					</div>
				</div>
				<!-- //item -->

				<!-- item -->
				<div class="accordion-item">
					<div class="item-button">
						<div class="label">O que é bitcoin?</div>
						<div class="button">
							<div class="more-to-arrow">
								<div class="shape-1"></div>
								<div class="shape-2"></div>
							</div>
						</div>
					</div>

					<div class="item-content">
						<div class="content-title h3 dark-blue hidden-xs hidden-sm">O que é bitcoin?</div>
						<div class="item-text">
							<p>A maneira mais fácil de comprar bitcoins é através de corretoras, como a Modiax. Você abre a sua conta, transfere reais e troca por Bitcoins. Simples e seguro!</p>
							<a href="" class="btn orange square-w">Comprar bitcoins</a>
						</div>
					</div>
				</div>
				<!-- //item -->

				<!-- item -->
				<div class="accordion-item">
					<div class="item-button">
						<div class="label">Como comprar bitcoin?</div>
						<div class="button">
							<div class="more-to-arrow">
								<div class="shape-1"></div>
								<div class="shape-2"></div>
							</div>
						</div>
					</div>

					<div class="item-content">
						<div class="content-title h3 dark-blue hidden-xs hidden-sm">Como comprar bitcoin?</div>
						<div class="item-text">
							<p>A maneira mais fácil de comprar bitcoins é através de corretoras, como a Modiax. Você abre a sua conta, transfere reais e troca por Bitcoins. Simples e seguro!</p>
							<a href="" class="btn orange square-w">Comprar bitcoins</a>
						</div>
					</div>
				</div>
				<!-- //item -->

				<!-- item -->
				<div class="accordion-item">
					<div class="item-button">
						<div class="label">Como comprar bitcoin?</div>
						<div class="button">
							<div class="more-to-arrow">
								<div class="shape-1"></div>
								<div class="shape-2"></div>
							</div>
						</div>
					</div>

					<div class="item-content">
						<div class="content-title h3 dark-blue hidden-xs hidden-sm">Como comprar bitcoin?</div>
						<div class="item-text">
							<p>A maneira mais fácil de comprar bitcoins é através de corretoras, como a Modiax. Você abre a sua conta, transfere reais e troca por Bitcoins. Simples e seguro!</p>
							<a href="" class="btn orange square-w">Comprar bitcoins</a>
						</div>
					</div>
				</div>
				<!-- //item -->
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-lg-6 call-to-action">
				<div class="label">Ainda tem alguma dúvida?</div>
				<a href="" class="btn blue square-w w-280">Tire suas dúvidas online</a>
			</div>
		</div>
	</div>
</section>