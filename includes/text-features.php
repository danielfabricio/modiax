<section class="section text-features">
	<div class="container">
		<div class="row">
			<div class="block-header col-xs-12">
				<h3 class="h2 blue">Valores que vão<br/><strong>muito além de moedas! </strong></h3>
			</div>

			<div class="block-content">
				<div class="feature-text-item col-xs-12 col-sm-4">
					<div class="item-ico"><i class="icon-shield"></i></div>
					<div class="item-content">
						<h5 class="item-title h3"><span>Segurança,</span> o ponto de partida!</h5>
						<p class="item-text">Trabalhamos 24h, 7 dias por semana, incessantemente para você dormir tranquilo. Utilizamos cold wallets e também a BITGO, líder em serviços de segurança.</p>
						<a href="" class="item-link">Saiba mais<i class="icon-arrow"></i></a>
					</div>
				</div>
				<div class="feature-text-item col-xs-12 col-sm-4">
					<div class="item-ico"><i class="icon-diamond"></i></div>
					<div class="item-content">
						<h5 class="item-title h3"><span>Profissionais,</span> não entusiastas!</h5>
						<p class="item-text">Fazemos parte do grupo Mosaico Digital Assets, que possui os maiores especialistas do mercado financeiro, jurídico e de criptomoedas do país. </p>
						<a href="" class="item-link">Saiba mais<i class="icon-arrow"></i></a>
					</div>
				</div>
				<div class="feature-text-item col-xs-12 col-sm-4">
					<div class="item-ico"><i class="icon-chat"></i></div>
					<div class="item-content">
						<h5 class="item-title h3"><span>Ao seu lado,</span> sempre!</h5>
						<p class="item-text">Seja o que for, a hora que for, estaremos sempre prontos. Câmbio! Mas nunca desligamos!</p>
						<a href="" class="item-link">Saiba mais<i class="icon-arrow"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>