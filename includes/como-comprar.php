<section class="section como-comprar">
	<div class="container">
		<div class="row">
			<div class="animation-arrow hidden-sm"></div>
			<div class="col-xs-12 col-lg-3  block-header">
				<h3 class="title h2">Como comprar?</h3>
				<p class="subtitle">Três passos e você já pode começar a investir!</p>
				<div class="arrow-animate hidden-sm">
					<div class="hidden-xs label">Vamos lá!</div>
					<div class="arrow">
						<i>
							<img src="images/arrow.svg" alt="">
						</i>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-lg-3">
				<div class="como-comprar-item">
					<div class="item-number">1</div>
					<div class="item-img"><img src="images/mac.svg" alt=""></div>
					<h4 class="title">Crie sua conta</h4>
					<p class="item-text">Crie uma conta gratuitamente e verifique seus dados.</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-lg-3">
				<div class="como-comprar-item">
					<div class="item-number">2</div>
					<div class="item-img"><img src="images/money.svg" alt=""></div>
					<h4 class="title">Deposite reais</h4>
					<p class="item-text">Faça uma transferência e carregue sua carteira Modiax com saldo em Reais.</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-lg-3">
				<div class="como-comprar-item">
					<div class="item-number">3</div>
					<div class="item-img"><img src="images/bitcoin.svg" alt=""></div>
					<h4 class="title">Compre bitcoins</h4>
					<p class="item-text">Utilize o seu saldo em Reais para comprar Bitcoins.</p>
				</div>
			</div>
		</div>
	</div>
</section>