<section class="section grafico">
	<div class="container">
		<div class="row">
			<div class="block-header col-xs-12">
				<div class="title h2 blue"><strong>Bitcoin</strong>, uma nova forma de investir</div>
				<p class="subtitle">Desde a sua criação, em 2009, a moeda já valorizou mais de 900 vezes.</p>
			</div>

			<div class="block-content col-xs-12">
				<div class="grafico-container">
					<div class="the-chart">
						<img src="images/grafico.png" alt="Gráfico de bitcoins">

						<div class="chart-bullet" style="left: 60%; bottom: 7%;">
							<div class="label top">
								<i class="line"></i>
								<div class="label-content">
									<div class="label-box">
										<div class="date">Fev 2018</div>
										<p>O mercado volta a subir e o preço bate  U$ 9.000.</p>
									</div>
								</div>
							</div>
						</div>


						<div class="chart-bullet" style="left: 40%; bottom: 7%;">
							<div class="label top">
								<i class="line"></i>
								<div class="label-content">
									<div class="label-box">
										<div class="date">Fev 2018</div>
										<p>O mercado volta a subir e o preço bate  U$ 9.000.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>