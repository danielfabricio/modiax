<section class="section simulador-bottom">
	<div class="container">
		<div class="row">
				<div class="simulador-text">
					<div class="text">
						Se você comprasse 
						<div class="text-input valor">R$ <input type="text" size='' id="money_val" class="number-mask width-dynamic proba dva" value="500"></div> 
						em bitcoins em 
						<div class="text-input ano">
							<div class="year-value">2010</div>
							<div class="more-year"><i class="icon-right-arrow"></i></div>
							<div class="less-year"><i class="icon-right-arrow"></i></div>

							<div class="dropdown active">
								<div class="item">2010</div>
								<div class="item">2011</div>
								<div class="item">2012</div>
								<div class="item">2013</div>
								<div class="item">2014</div>
								<div class="item">2015</div>
								<div class="item">2016</div>
								<div class="item">2017</div>
								<div class="item">2018</div>
							</div>
						</div>, hoje você teria aproximadamente <span class="result">R$ 4.325,92</span>.</div>
				</div>

				<div class="call-to-action clear">
					<span class="subtitle">Nunca é tarde para começar!</span>
					<a href="" class="btn orange square-w">Comprar bitcoins</a>
				</div>
		</div>
	</div>
</section>