<div class="simulador">
	<h4 class="title twhite hidden-xs">Simule agora!</h4>

	<div class="simulador-box">
		<div class="simulador-type clear">
			<div class="type-item active" data-type="comprar">Comprar</div>
			<div class="type-item" data-type="vender">Vender</div>
		</div>

		<div class="simulador-form">
			<form action="">
				<div class="comprar-tab active">
					<div class="simulador-input">
						<label for="simulador_top_valor">Valor em reais</label>
						<input type="text" class="input money-mask" id="simulador_top_valor" name="valor" placeholder="0,00">
						<span class="money-simbol">R$</span>
					</div>
					<div class="simulador-input">
						<label for="simulador_top_qtd">Quantidade em bitcoins</label>
						<input type="text" class="input bitcoin-mask" id="simulador_top_qtd" name="quantidade" placeholder="0.0">
						<span class="money-simbol">Ƀ</span>
					</div>
				</div>

				<div class="vender-tab" style="display:none;">
					<div class="simulador-input">
						<label for="simulador_top_qtd">Quantidade em bitcoins</label>
						<input type="text" class="input bitcoin-mask" id="simulador_top_qtd" name="quantidade" placeholder="0.0">
						<span class="money-simbol">Ƀ</span>
					</div>
					<div class="simulador-input">
						<label for="simulador_top_valor">Valor em reais</label>
						<input type="text" class="input money-mask" id="simulador_top_valor" name="valor" placeholder="0,00">
						<span class="money-simbol">R$</span>
					</div>
				</div>

				<div class="simulator-submit">
					<button class="btn orange square-w" type="submit">Comprar bitcoins</button>
				</div>

				<span class="text-bottom">
					Cotação atualizada a cada 60 segundos.
				</span>
			</form>
		</div>
	</div>
</div>