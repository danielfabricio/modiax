<?php $bodyclass = 'about default-page not-home'; ?>
<?php include('header.php'); ?>

	<!-- BG SVG -->
	<div class="top-bg-about">
		<div class="container">
			<div class="the_bg">
			</div>
		</div>
	</div>
	<!-- BG SVG -->

	<main class="main" role="main">
		<section class="section page-top-sec">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">

						<div class="top-label w-blue grey">
							<span>Do seu jeito!</span>
						</div>

						<h1 class="title">
							Prazer, somos a Mondiax
						</h1>

					</div>
				</div>
			</div>
		</section>

		<section class="main-content">
			<div class="container">
				<div class="row">
					
					<div class="page-secont-title col-xs-12">
						<h2 class="h2 blue title">Idealizada por especialistas, <span>construída para você!</span></h2>
					</div>

					<div class="about-desc col-xs-12 col-sm-4 col-lg-6">
						<p>Somos a combinação de um time de empreendedores de tecnologia com grandes especialistas do mercado financeiro. Juntos, compartilhamos o objetivo de criar uma plataforma de investimento centrada no investidor digital, que seja confiável, simples e principalmente, segura.</p>
					</div>

					<div class="about-content page-content col-xs-12 col-sm-8 col-lg-6">
						<p>Nascemos para os que acreditam que as criptomoedas e ativos digitais serão o futuro da economia.</p>
						<p>Para os que enxergam o seu potencial como oportunidade de investimento.</p>
						<p>Para os que buscam uma solução simples e transparente em suas negociações.</p>
						<p>Não queremos ser apenas o canal de ligação entre o investidor e o investimento. Nós te ajudamos do começo ao fim, tão próximos quanto um clique de distância, seja através da plataforma, ou com o suporte e conhecimento de um time de especialistas que se preocupa com você.</p>
						<p>Para tornar isso realidade, a sua participação é fundamental, pois você é o motivo da nossa existência. Nós fornecemos a base, você nos ajuda a moldar o produto de acordo com os seus interesses e necessidades (Veja como).</p>
						<br/>
						<div class="tagline">Aos iniciantes, ensinamos.<br/>Os profissionais, equipamos.</div>
					</div>

				</div>
			</div>
		</section>

		<section class="investidores">
			<div class="container">
				<div class="row">
					
					<div class="col-xs-12 col-lg-3 sec-title">
						<h3 class="title h2 blue">Nossos<br/>investidores</h3>
					</div>

					<div class="col-xs-12 col-lg-9 investidores-list">
						<div class="sub-row clear">
							<div class="investidores-item">
								<div class="item-img">
									<img src="images/Logo-Mosaico.svg" alt="Mosaico">
								</div>
								<p class="item-content">Grupo focado na construção e investimento de empresas de <strong>blockchain e ativos digitais</strong>. Conta com grandes nomes do mercado financeiro, jurídico, cibersegurança e de criptomoedas do Brasil.</p>
								<a href="" class="item-link">Mais sobre a Mosaico Digital Asstes <i class="icon-arrow"></i></a>
							</div>

							<div class="investidores-item">
								<div class="item-img">
									<img src="images/MAR-ventures-logo.svg" alt="MAR Ventures">
								</div>
								<p class="item-content">Venture Builder focada no desenvolvimento escalável de <strong>tecnologias de alto impacto</strong>. Possui um portfólio com seis empresas, quatro delas atuando na indústria financeira.</p>
								<a href="" class="item-link">Mais sobre a MAR ventures <i class="icon-arrow"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php include('includes/account-boxes.php'); ?>

	</main>

<?php include('footer.php'); ?>
